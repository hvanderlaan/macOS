#!/usr/bin/env python

''' mkpasswd.py - python script for creating sha512 hashed passwords '''

from __future__ import print_function
import sys
import platform
import string
import random
import getpass

if platform.system() == 'Darwin':
    try:
        from passlib.hash import sha512_crypt
    except ImportError:
        sys.stderr.write('[-]: macOS crypt function does not work properly.\n')
        sys.stderr.write('[-]: failover to passlib.hash but could not find it.\n')
        sys.exit(1)
elif platform.system() == 'Linux':
    import crypt
else:
    sys.stderr.write('[-]: operating system not supported.\n')
    sys.exit(1)

def gen_random_salt(charset):
    ''' function for generating random salt of 8 characters '''
    return ''.join(random.sample(charset*8, 8))

def gen_sha512_hash(password, salt):
    ''' function to generate sha512 password hash '''
    if platform.system() == 'Darwin':
        sha512hash = sha512_crypt.encrypt(password, salt=salt, rounds=5000)
    elif platform.system() == 'Linux':
        sha512hash = crypt.crypt(password, '$6${}$' .format(salt))
    else:
        pass

    return sha512hash

def main():
    ''' main function '''
    charset = string.ascii_lowercase + string.ascii_uppercase + string.digits
    salt = gen_random_salt(charset)
    password = getpass.getpass('[*] please enter a password: ')
    sha512hash = gen_sha512_hash(password, salt)

    print('\n{}' .format(sha512hash))

if __name__ == "__main__":
    main()
    sys.exit(0)
